Feature: Login Evermos

Background:
    Given User has opened the browser

  Scenario: Login with valid credential 
    Given User go to Evermos Login Page
    When User input phone number <Phone>
    And User input password <Password>
    And User click button Login
    Then User successfully logged in
    And User redirected to Dashboard page

  Scenario: Login with invalid credential - valid phone and invalid password
    Given User go to Evermos Login Page
    When User input phone number <Phone>
    And User input password <Password>
    And User click button Login
    Then User successfully logged in
    And User redirected to Dashboard page 

  Scenario: Login with invalid credential - invalid phone and invalid password
    Given User go to Evermos Login Page
    When User input phone number <Phone>
    And User input password <Password>
    And User click button Login
    Then User successfully logged in
    And User redirected to Dashboard page
  
  Scenario: Login with invalid credential - valid phone and empty password
    Given User go to Evermos Login Page
    When User input phone number <Phone>
    And User input password <Password>
    And User click button Login
    Then User successfully logged in
    And User redirected to Dashboard page

  Scenario: Login with invalid credential - empty phone and empty password
    Given User go to Evermos Login Page
    When User input phone number <Phone>
    And User input password <Password>
    And User click button Login
    Then User successfully logged in
    And User redirected to Dashboard page 