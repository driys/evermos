Feature: Search Product Evermos

Background:
    Given User Already Login

 
  Scenario: Search product using valid input
    Given User Click Search Field
    When User input valid Product <Product> on search field
    And User hit button Enter
    Then Show all product based on search result

  Scenario: Search product using invalid input
    Given User Click Search Field
    When User input valid Product <Product> on search field
    And User hit button Enter
    Then No Product appear in the search result
    And Show message Hasil Pencarianmu Tidak Ada